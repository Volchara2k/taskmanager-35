<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../include/_header.jsp"/>

<c:choose>
    <c:when test="${not empty task.title}">

    <h2>Edit task: &laquo;${task.title}&raquo;</h2>
    <form:form action="/task/edit/${task.id}" method="POST" modelAttribute="task">

        <form:input type="hidden" path="id"/>
        <form:input type="hidden" path="userId"/>

        <p>
            <div>Title:</div>
            <div><form:input type="text" path="title"/></div>
        </p>

        <p>
            <div>Description:</div>
            <div><form:input type="text" path="description"/></div>
        </p>

        <button type="submit">SAVE</button>
    </form:form>

    </c:when>
    <c:otherwise>

    <h2>Create task</h2>
    <form:form action="/task/create/" method="POST" modelAttribute="task">

        <form:input type="hidden" path="id"/>
        <form:input type="hidden" path="userId"/>

        <p>
            <div>Title:</div>
            <div><form:input type="text" path="title"/></div>
        </p>

        <p>
            <div>Description:</div>
            <div><form:input type="text" path="description"/></div>
        </p>

        <button type="submit">SAVE</button>

    </form:form>
    </c:otherwise>

</c:choose>

<jsp:include page="../include/_footer.jsp"/>``