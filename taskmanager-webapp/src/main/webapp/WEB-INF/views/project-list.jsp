<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../include/_header.jsp"/>

<h2>Project list:</h2>

<table>

    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">TITLE</th>
        <th width="100%">DESCRIPTION</th>
        <th width="100" nowrap="nowrap">EDIT</th>
        <th width="100" nowrap="nowrap">DELETE</th>
    </tr>

    <c:forEach var="project" items="${projects}">
        <tr>

            <td>
                <c:out value="${project.id}" />
            </td>

            <td>
                <c:out value="${project.title}" />
            </td>

            <td>
                <c:out value="${project.description}"/>
            </td>

            <td>
                <a href="/project/edit/${project.id}"><button>Edit</button></a>
            </td>

            <td>
                <a href="/project/delete/${project.id}"><button>Delete</button></a>
            </td>

        </tr>
    </c:forEach>

</table>

<a href="${pageContext.request.contextPath}/project/create/">
    <button>CREATE</button>
</a>

<jsp:include page="../include/_footer.jsp"/>