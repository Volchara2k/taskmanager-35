<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../include/_header.jsp"/>

<h2>Task list</h2>

<table>

    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">TITLE</th>
        <th width="100%">DESCRIPTION</th>
        <th width="100" nowrap="nowrap">EDIT</th>
        <th width="100" nowrap="nowrap">DELETE</th>
    </tr>

    <c:forEach var="task" items="${tasks}">
        <tr>

            <td>
                <c:out value="${task.id}" />
            </td>

            <td>
                <c:out value="${task.title}" />
            </td>

            <td>
                <c:out value="${task.description}" />
            </td>

            <td>
                <a href="/task/edit/${task.id}"><button>Edit</button></a>
            </td>

            <td>
                <a href="/task/delete/${task.id}"><button>Delete</button></a>
            </td>

        </tr>
    </c:forEach>

</table>

<a href="${pageContext.request.contextPath}/task/create/">
    <button>CREATE</button>
</a>

<jsp:include page="../include/_footer.jsp"/>