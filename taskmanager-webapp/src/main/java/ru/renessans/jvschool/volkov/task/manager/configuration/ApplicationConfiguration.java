package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.renessans.jvschool.volkov.task.manager")
public class ApplicationConfiguration {
}