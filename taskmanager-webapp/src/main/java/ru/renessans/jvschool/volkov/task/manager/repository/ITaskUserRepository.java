package ru.renessans.jvschool.volkov.task.manager.repository;

import ru.renessans.jvschool.volkov.task.manager.model.Task;

public interface ITaskUserRepository extends IOwnerUserRepository<Task> {
}