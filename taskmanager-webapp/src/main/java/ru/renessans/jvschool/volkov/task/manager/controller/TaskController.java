package ru.renessans.jvschool.volkov.task.manager.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

import java.util.Objects;

@Controller
@RequiredArgsConstructor
public class TaskController {

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView index() {
        return new ModelAndView("task-list", "tasks", this.taskUserService.exportOwnerUser());
    }

    @NotNull
    @GetMapping("/task/create")
    public ModelAndView create() {
        return new ModelAndView("task-settable", "task", new Task());
    }

    @NotNull
    @PostMapping("/task/create")
    public String create(
            @ModelAttribute("task") @NotNull final Task task,
            @NotNull final BindingResult result
    ) {
        this.taskUserService.addOwnerUser(task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") @NotNull final String id
    ) {
        this.taskUserService.deleteRecordById(id);
        return "redirect:/tasks";
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Task task = taskUserService.getRecordById(id);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        return new ModelAndView("task-settable", "task", task);
    }

    @NotNull
    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") @NotNull final Task task,
            @NotNull final BindingResult result
    ) {
        this.taskUserService.addOwnerUser(task);
        return "redirect:/tasks";
    }

}