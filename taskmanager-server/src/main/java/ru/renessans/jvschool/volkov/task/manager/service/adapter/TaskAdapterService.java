package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.TimeFrame;

import java.util.Objects;

@Service
public final class TaskAdapterService implements ITaskAdapterService {

    @Nullable
    @Override
    public TaskDTO toDTO(@Nullable final Task convertible) {
        if (Objects.isNull(convertible)) return null;
        return TaskDTO.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .creationDate(convertible.getTimeFrame().getCreationDate())
                .build();
    }

    @Nullable
    @Override
    public Task toModel(@Nullable final TaskDTO convertible) {
        if (Objects.isNull(convertible)) return null;
        return Task.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .timeFrame(new TimeFrame(convertible.getCreationDate()))
                .build();
    }

}