package ru.renessans.jvschool.volkov.task.manager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwner extends AbstractModel {

    @NotNull
    @Column(
            nullable = false,
            length = 50
    )
    private String title = "";

    @NotNull
    @Column(
            nullable = false,
            length = 150
    )
    private String description = "";

    @Nullable
    @Column
    private String userId;

    @NotNull
    private TimeFrame timeFrame = new TimeFrame();

    @Nullable
    @ManyToOne
    private User user;

}