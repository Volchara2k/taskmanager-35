package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.SneakyThrows;
import lombok.Synchronized;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;

import java.util.Scanner;

@UtilityClass
public final class ScannerUtil {

    @NotNull
    private static final Scanner SCANNER = new Scanner(System.in);

    @Synchronized
    @NotNull
    public String getLine() {
        return SCANNER.nextLine();
    }

    @Synchronized
    @NotNull
    @SneakyThrows
    public Integer getInteger() {
        @NotNull final String integerLine = getLine();
        try {
            return Integer.parseInt(integerLine);
        } catch (@NotNull final Exception e) {
            throw new IllegalIndexException(integerLine);
        }
    }

}