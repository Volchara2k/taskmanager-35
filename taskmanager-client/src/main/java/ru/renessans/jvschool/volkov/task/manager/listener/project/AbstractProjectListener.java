package ru.renessans.jvschool.volkov.task.manager.listener.project;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;

@RequiredArgsConstructor
public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    protected final ProjectEndpoint projectEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

}