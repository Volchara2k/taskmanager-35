package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dataBinClearResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="dataBinClearResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="clearedBinData" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dataBinClearResponse", propOrder = {
        "clearedBinData"
})
public class DataBinClearResponse {

    protected boolean clearedBinData;

    /**
     * Gets the value of the clearedBinData property.
     */
    public boolean isClearedBinData() {
        return clearedBinData;
    }

    /**
     * Sets the value of the clearedBinData property.
     */
    public void setClearedBinData(boolean value) {
        this.clearedBinData = value;
    }

}
